package HireEngine.integration.rest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import HireEngine.Application;
import HireEngine.SimpleDbConfig;
import HireEngine.Resources.POResource;
import HireEngine.Resources.PlantResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class,
		SimpleDbConfig.class })
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class POControllerIntegrationTest {
	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;
	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@SuppressWarnings("deprecation")
	@Test
	@DatabaseSetup(value = "dataset.xml")
	public void testCreatePurchaseOrder() throws Exception {
		PlantResource plant = new PlantResource();
		plant.setIdRes(10001L);
		plant.setName("Excavator");
		plant.setPrice(300f);
		plant.setDescription("New Plant");
		plant.add(new Link("http://localhost:8080/rest/plants/10001"));
		plant.setPrice(300f);

		POResource poResource = new POResource();
		poResource.setPlantResource(plant);
		poResource.setStartDate(new Date(2014, 10, 6));
		poResource.setEndDate(new Date(2014, 10, 10));

		MvcResult result = mockMvc
				.perform(
						post("/rest/pos").contentType(
								MediaType.APPLICATION_JSON).content(
								mapper.writeValueAsString(poResource)))
				.andExpect(status().isCreated()).andReturn();

		POResource poR = mapper.readValue(result.getResponse()
				.getContentAsString(), POResource.class);
		assertThat(poR.getLink("self"), is(notNullValue()));
		assertThat(poR.get_link("acceptPO"), is(notNullValue()));
		assertThat(poR.get_link("rejectPO"), is(notNullValue()));
		assertThat(poR.getLinks().size() + poR.get_links().size(), is(3));
	}

	@Test
	@DatabaseSetup(value = "DatabaseWithPendingPO.xml")
	public void testAcceptPurchaseOrder() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/pos/{id}", 10001L))
				.andExpect(status().isOk()).andReturn();
		POResource poResource = mapper.readValue(result.getResponse()
				.getContentAsString(), POResource.class);
		assertThat(poResource.get_link("acceptPO"), is(notNullValue()));

		Link approve = poResource.get_link("acceptPO");

		result = mockMvc.perform(post(approve.getHref()))
				.andExpect(status().isOk()).andReturn();

		poResource = mapper.readValue(
				result.getResponse().getContentAsString(), POResource.class);
		assertThat(poResource.getLink("self"), is(notNullValue()));
		assertThat(poResource.getLink("closePO"), is(notNullValue()));
		assertThat(poResource.getLink("requestPOUpdate"), is(notNullValue()));
		assertThat(
				poResource.getLinks().size() + poResource.get_links().size(),
				is(3));
	}
}
