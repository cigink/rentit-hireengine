package HireEngine.Assembler;

import java.util.ArrayList;
import java.util.List;

import HireEngine.Models.Plant;
import HireEngine.Resources.PlantResource;

public class PlantAssembler {

	public PlantResource toResource(Plant plant) {
		PlantResource res = new PlantResource();

		res.setName(plant.getName());
		res.setDescription(plant.getDescription());
		res.setPrice(plant.getPrice());
		return res;
	}

	public List<PlantResource> toResource(List<Plant> plants) {
		List<PlantResource> ress = new ArrayList<>();

		for (Plant plant : plants) {
			ress.add(toResource(plant));
		}
		return ress;
	}
}
