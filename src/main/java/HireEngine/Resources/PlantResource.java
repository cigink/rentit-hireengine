package HireEngine.Resources;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import HireEngine.SupportClass.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name = "plant")
public class PlantResource extends ResourceSupport {

	Long idRes;
	String Name;
	String Description;
	Float Price;

}
