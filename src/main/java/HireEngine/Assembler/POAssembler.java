package HireEngine.Assembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import HireEngine.Models.PurchaseOrder;
import HireEngine.Resources.POResource;
import HireEngine.SupportClass.ExtendedLink;
import HireEngine.integration.rest.POController;

public class POAssembler extends
		ResourceAssemblerSupport<PurchaseOrder, POResource> {

	PlantAssembler plantAssembler;

	public POAssembler() {
		super(POController.class, POResource.class);
	}

	@Override
	public POResource toResource(PurchaseOrder po) {
		POResource poResource = createResourceWithId(po.getId(), po);
		poResource.setCost(po.getCost());
		poResource.setEndDate(po.getEndDate());
		PlantAssembler assem = new PlantAssembler();
		poResource.setPlantResource(assem.toResource(po.getPlant()));
		poResource.setStartDate(po.getStartDate());

		switch (po.getStatus()) {
		case PENDING_CONFIRMATION:
			poResource.add(new ExtendedLink(linkTo(
					methodOn(POController.class).rejectPO(po.getId()))
					.toString(), "rejectPO", "DELETE"));
			poResource.add(new ExtendedLink(linkTo(
					methodOn(POController.class).acceptPO(po.getId()))
					.toString(), "acceptPO", "POST"));
			break;
		case PENDING_UPDATE:
			poResource.add(new ExtendedLink(linkTo(
					methodOn(POController.class).rejectPOUpdate(po.getId()))
					.toString(), "rejectPOUpdate", "DELETE"));
			poResource.add(new ExtendedLink(linkTo(
					methodOn(POController.class).acceptPOUpdate(po.getId()))
					.toString(), "acceptPOUpdate", "POST"));
			break;
		case REJECTED:
			poResource.add(new ExtendedLink(linkTo(
					methodOn(POController.class).updatePO(po.getId()))
					.toString(), "updatePO", "UPDATE"));
			break;
		case CLOSED:
			break;
		case OPEN:
			poResource.add(new ExtendedLink(linkTo(
					methodOn(POController.class).closePO(po.getId()))
					.toString(), "closePO", "DELETE"));
			poResource
					.add(new ExtendedLink(linkTo(
							methodOn(POController.class).requestPOUpdate(
									poResource, po.getId())).toString(),
							"requestPOUpdate", "POST"));
		default:
			break;
		}

		return poResource;
	}

	public List<POResource> toResource(List<PurchaseOrder> pos) {
		List<POResource> poress = new ArrayList<>();

		for (PurchaseOrder po : pos) {
			poress.add(toResource(po));
		}
		return poress;
	}

}
