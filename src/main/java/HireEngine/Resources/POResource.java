package HireEngine.Resources;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import HireEngine.Models.Customer;
import HireEngine.SupportClass.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name = "purchase_order")
public class POResource extends ResourceSupport {

	Long idPo;
	String Name;
	@Temporal(TemporalType.DATE)
	Date startDate;

	@Temporal(TemporalType.DATE)
	Date endDate;
	Float Cost;
	PlantResource plantResource;
	Customer customer;

}
