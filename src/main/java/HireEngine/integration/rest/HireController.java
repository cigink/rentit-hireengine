package HireEngine.integration.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import HireEngine.Assembler.PlantAssembler;
import HireEngine.Exception.PlantNotFoundException;
import HireEngine.Models.Plant;
import HireEngine.Resources.PlantResource;
import HireEngine.repositiories.PlantRepository;

@RestController
@RequestMapping(value = "/rest/plants")
public class HireController {

	@Autowired
	PlantRepository repo;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<PlantResource> getAll() {
		List<Plant> plants = repo.findAll();
		PlantAssembler assembler = new PlantAssembler();
		List<PlantResource> res = assembler.toResource(plants);
		return res;
	}

	@RequestMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PlantResource getPlant(@PathVariable("id") Long id) throws Exception {
		Plant plant = repo.findOne(id);
		if (plant == null) {
			throw new PlantNotFoundException(id);
		}
		PlantAssembler assembler = new PlantAssembler();
		return assembler.toResource(plant);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public PlantResource create(@RequestBody PlantResource pres)
			throws PlantNotFoundException {
		if (pres == null) {
			return null;
		}
		Plant plant = new Plant();
		plant.setName(pres.getName());
		plant.setDescription(pres.getDescription());
		plant.setPrice(pres.getPrice());

		Plant createdPlant = repo.saveAndFlush(plant);
		PlantAssembler assembler = new PlantAssembler();
		return assembler.toResource(createdPlant);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public PlantResource updatePlant(@PathVariable("id") Long id)
			throws Exception {
		Plant plant = repo.findOne(id);
		if (plant == null) {
			throw new PlantNotFoundException(id);
		}
		Plant newPlant = repo.saveAndFlush(plant);
		PlantAssembler assembler = new PlantAssembler();
		return assembler.toResource(newPlant);
	}

	// @RequestMapping("/plants")
	// public String list(Model model) {
	// model.addAttribute("plants", repo.findAll());
	// return "plants/list";
	// }
	//
	// @RequestMapping(value = "/plants/form")
	// public String form(Model model) {
	// model.addAttribute("plant", new Plant());
	// return "plants/create";
	// }
	//
	// @RequestMapping(value = "/plants", method = RequestMethod.POST)
	// public String create(Plant plant) {
	// repo.saveAndFlush(plant);
	// return "redirect:/plants";
	// }
	//
	// @RequestMapping("/plants/{id}")
	// public String show(Model model, @PathVariable Long id) {
	// model.addAttribute(id);
	// return "plants/{id}";
	// }
	//
	// @RequestMapping("/plants/{id}/form")
	// public String edit(Model model, @PathVariable Long id) {
	// model.addAttribute(repo.findOne(id));
	// return "plants/edit";
	//
	// }
	//
	// @RequestMapping(value = "/plants/{id}", method = RequestMethod.PUT)
	// public String update(Plant plant, @PathVariable Long id) {
	// repo.saveAndFlush(plant);
	// return "redirect:/plants";

	@ExceptionHandler(PlantNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPlantNotFoundException(PlantNotFoundException ex) {
	}

}
