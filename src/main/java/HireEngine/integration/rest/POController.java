package HireEngine.integration.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import HireEngine.Assembler.POAssembler;
import HireEngine.Exception.PONotFoundException;
import HireEngine.Exception.PlantNotFoundException;
import HireEngine.Models.Plant;
import HireEngine.Models.PurchaseOrder;
import HireEngine.Models.Status;
import HireEngine.Resources.POResource;
import HireEngine.Resources.PlantResource;
import HireEngine.repositiories.PORespository;
import HireEngine.repositiories.PlantRepository;

@RestController
@RequestMapping(value = "/rest/pos")
public class POController {

	@Autowired
	PORespository poRepo;

	@Autowired
	PlantRepository plantRepo;

	@RequestMapping(value = "/getPO/{id}")
	@ResponseStatus(HttpStatus.OK)
	public POResource getPO(@PathVariable("id") Long id) throws Exception {
		PurchaseOrder fpo = poRepo.findOne(id);
		if (fpo == null) {
			throw new PONotFoundException(id);
		}
		POAssembler assemb = new POAssembler();
		POResource res = assemb.toResource(fpo);
		return res;
	}

	@RequestMapping(value = "/getAll")
	public List<POResource> getAll() {
		List<PurchaseOrder> pos = poRepo.findAll();
		POAssembler assemb = new POAssembler();
		List<POResource> resources = assemb.toResource(pos);
		return resources;
	}

	@RequestMapping(value = "/{po.id}/", method = RequestMethod.POST)
	public ResponseEntity<POResource> updatePO(@RequestBody POResource res,
			@PathVariable("id") Long id) throws PONotFoundException {

		ResponseEntity<POResource> response;

		if (res == null) {
			response = new ResponseEntity<POResource>(HttpStatus.NOT_FOUND);
			return response;
		}

		if (poRepo.findOne(res.getIdPo()) == null) {
			throw new PONotFoundException(res.getIdPo());
		}

		PurchaseOrder po = new PurchaseOrder();
		po.setStartDate(res.getStartDate());
		po.setEndDate(res.getEndDate());
		if (res.getPlantResource() != null) {
			PlantResource resource = res.getPlantResource();
			Plant plant = new Plant();
			plant.setId(resource.getIdRes());
			plant.setName(resource.getName());
			plant.setDescription(resource.getDescription());
			plant.setPrice(resource.getPrice());
			po.setPlant(plant);
		}

		po.setCustomer(res.getCustomer());
		po.calcCost(po.getPlant());
		po.setId(res.getIdPo());
		poRepo.delete(id);
		po.setStatus(Status.PENDING_CONFIRMATION);
		poRepo.saveAndFlush(po);
		response = new ResponseEntity<POResource>(res, HttpStatus.OK);
		return response;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<POResource> createPO(@RequestBody POResource res)
			throws PlantNotFoundException {

		ResponseEntity<POResource> resp;
		if (res == null) {
			throw new PlantNotFoundException(null);
		}
		PurchaseOrder po = new PurchaseOrder();
		po.setStartDate(res.getStartDate());
		po.setEndDate(res.getEndDate());
		if (res.getPlantResource() != null) {
			PlantResource resource = res.getPlantResource();
			Plant plant = new Plant();
			plant.setId(resource.getIdRes());
			plant.setName(resource.getName());
			plant.setDescription(resource.getDescription());
			plant.setPrice(resource.getPrice());
			po.setPlant(plant);
		}
		if (plantRepo.isPlantAvailable(po.getPlant().getName(),
				po.getStartDate(), po.getEndDate()).isEmpty()) {
			// Throw exception if plant NOT available
			throw new PlantNotFoundException(po.getPlant().getId());
		}
		// Compute the total cost of the hiring
		po.calcCost(po.getPlant());
		po.setStatus(Status.PENDING_CONFIRMATION);
		PurchaseOrder newPo = poRepo.saveAndFlush(po);
		// Return CREATED status after PO is successfully created
		POAssembler assemb = new POAssembler();
		POResource newRes = assemb.toResource(newPo);
		resp = new ResponseEntity<POResource>(newRes, HttpStatus.CREATED);
		return resp;
	}

	@RequestMapping(value = "/{id}/accept", method = RequestMethod.DELETE)
	public ResponseEntity<Void> rejectPO(@PathVariable("id") Long id) {
		PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.REJECTED);
		poRepo.delete(id);
		poRepo.saveAndFlush(po);
		ResponseEntity<Void> resp = new ResponseEntity<Void>(HttpStatus.OK);
		return resp;
	}

	@RequestMapping(value = "/{id}/accept", method = RequestMethod.POST)
	public ResponseEntity<Void> acceptPO(@PathVariable("id") Long id) {
		PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.OPEN);
		poRepo.delete(id);
		poRepo.saveAndFlush(po);
		ResponseEntity<Void> resp = new ResponseEntity<Void>(HttpStatus.OK);
		return resp;
	}

	@RequestMapping(value = "/{po.id}/updates/{up.id}/accept", method = RequestMethod.DELETE)
	public ResponseEntity<POResource> rejectPOUpdate(@RequestBody Long id) {
		return null;
	}

	@RequestMapping(value = "/{po.id}/updates/{up.id}/accept", method = RequestMethod.POST)
	public ResponseEntity<POResource> acceptPOUpdate(@RequestBody Long id) {
		return null;
	}

	@RequestMapping(value = "/{po.id}/updates", method = RequestMethod.PUT)
	public ResponseEntity<POResource> updatePO(@RequestBody Long id) {
		return null;
	}

	@RequestMapping(value = "/{po.id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> closePO(@PathVariable("id") Long id) {
		PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.CLOSED);
		poRepo.delete(id);
		poRepo.saveAndFlush(po);
		ResponseEntity<Void> resp = new ResponseEntity<Void>(HttpStatus.OK);
		return resp;
	}

	@RequestMapping(value = "/{po.id}/updates", method = RequestMethod.POST)
	public ResponseEntity<POResource> requestPOUpdate(
			@RequestBody POResource res, @PathVariable("id") Long id) {
		PurchaseOrder po = new PurchaseOrder();
		po.setStartDate(res.getStartDate());
		po.setEndDate(res.getEndDate());
		PlantResource plantRes = res.getPlantResource();
		Plant plant = new Plant();
		plant.setName(plantRes.getName());
		plant.setDescription(plantRes.getDescription());
		plant.setPrice(plantRes.getPrice());
		po.setPlant(plant);
		po.setCustomer(res.getCustomer());
		po.calcCost(plant);
		poRepo.delete(id);
		po.setStatus(Status.PENDING_CONFIRMATION);
		poRepo.saveAndFlush(po);
		new ResponseEntity<POResource>(res, HttpStatus.OK);
		return null;
	}

	@ExceptionHandler(PONotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPONotFoundException(PONotFoundException ex) {
	}
}
